FROM postgres:14 as motorway-test-backend
WORKDIR /app
COPY ./scripts/init.sh /docker-entrypoint-initdb.d
COPY ./scripts/dump.sql ./scripts/motorway-test-backend/dump.sql


FROM node:18-alpine As development
RUN npm set progress=false && npm config set depth 0

WORKDIR /usr/src/app

COPY tsconfig.build.json ./

COPY package*.json ./

RUN npm install
COPY . .
RUN npm run build
EXPOSE 7070

FROM node:18-alpine As release
RUN npm set progress=false && npm config set depth 0
ARG NODE_ENV=production
ENV NODE_ENV=production

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install
COPY tsconfig.build.json ./

RUN npm install --only=production

COPY . .

# COPY --from=development /usr/src/app/dist ./dist

EXPOSE 7070