import { AppDataSource } from "../db/data-source";
import { StateLogs } from "../entities/stateLog.entity";
import { Vehicle } from "../entities/vehicle.entity";

type TVehicle = {
  id: number;
  make: string;
  model: string;
  timestamp: Date;
  state: string;
};

const stateLogsRepo = AppDataSource.getRepository(StateLogs);
export const vehicleRepo = AppDataSource.getRepository(Vehicle);

const VehicleService = {
  async getVehicleStatus(vehicle: Vehicle, timestamp: Date): Promise<TVehicle> {
    const stateLogs = await stateLogsRepo.find({
      where: {
        vehicleId: vehicle.id,
      },
      order: { timestamp: "DESC" },
    });

    const states = stateLogs.filter(
      (stateLog) => stateLog.timestamp <= timestamp
    );

    let state = "unknown";

    if (states.length > 0) {
      state = states[0].state;
    }
    return {
      id: vehicle.id,
      make: vehicle.make,
      model: vehicle.model,
      timestamp: timestamp,
      state: state,
    };
  },
};

export default VehicleService;
