import "reflect-metadata";

import Config from "../config/config";
import { join } from "path";
import { DataSource } from "typeorm";
import "reflect-metadata";

const dbUrl = Config.db.url;

export const AppDataSource = new DataSource({
  type: "postgres",
  migrationsRun: false,
  migrationsTableName: "migrations",
  synchronize: false,
  logging: true,
  entities: [join(__dirname, "../", "/**/*.entity{.ts,.js}")],
  migrations: [join(__dirname, "/migrations/**/*{.ts,.js}")],
  subscribers: [],
  database: Config.db.name,
  ...(dbUrl
    ? { url: dbUrl }
    : {
        host: Config.db.host,
        port: Config.db.port,
        username: Config.db.username,
        password: Config.db.password,
      }),
});

AppDataSource.initialize()
  .then(() => {
    console.log("Data Source has been initialized!");
  })
  .catch((err) => {
    console.error("Error during Data Source initialization", err);
  });
