const { env } = process;

const isProd = env.NODE_ENV === "production";
const isTest = env.NODE_ENV === "test";

if (isTest)
  require("dotenv").config({
    path: ".env.test",
  });

if (!isProd && !isTest) require("dotenv").config();

export default {
  isProd,
  logLevel: env.LOG_LEVEL ? env.LOG_LEVEL : "info",
  port: parseInt(env.PORT) || 7070,
  db: {
    url: env.DATABASE_URL,
    host: env.DB_HOST,
    port: parseInt(env.DB_PORT || "5432"),
    username: env.DB_USER,
    password: env.DB_PASS,
    name: env.DB_NAME,
  },
};
