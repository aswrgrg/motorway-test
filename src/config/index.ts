export default {
  baseSecret: process.env.BASE_SECRET,
  logLevel: process.env.LOG_LEVEL || "info",
  nodeEnv: process.env.NODE_ENV || "development",
  isProd: process.env.NODE_ENV === "production",
  isTest: process.env.NODE_ENV === "test",
  envFile: process.env.NODE_ENV ? `.env.${process.env.NODE_ENV}` : ".env",
  appPort: parseInt(process.env.APP_PORT || "7070"),
  siteUrl:
    process.env.SITE_HOST || process.env.SITE_URL || process.env.ROOT_URL,
  httpProtocol: process.env.NODE_ENV === "production" ? "https:" : "http:",
};
