import app from "./app";
import config from "./config";

const a = new app();

if (!config.isProd) {
  a.loadEnv();
}

a.healthChk();
a.applyJsonMiddleWares();
a.loadRoutes();

const expressCore = a.core;

const server = expressCore.listen(config.appPort, "0.0.0.0", () => {
  console.log(`Server started on 0.0.0.0:${config.appPort}`);
});
