import joi from "joi";
import { NextFunction, Request, Response } from "express";

export const JoiValidator = (schema: joi.Schema, type = "body") => {
  return (req: Request, res: Response, next: NextFunction) => {
    const { error, value } = schema.validate(req[type], { abortEarly: false });

    if (error) {
      return res.status(422).json({
        errors: error.details.map(({ message, path }) => ({ message, path })),
      });
    }

    req[type] = value;

    next();
  };
};

export default JoiValidator;
