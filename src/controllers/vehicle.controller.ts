import { ControllerActionIface } from "./base";
import { Request, Response } from "express";
import VehicleService, { vehicleRepo } from "../services/vehicle.service";
import joi from "joi";

export const findStateHandler: ControllerActionIface = {
  querySchema: joi.object().keys({
    vehicle_id: joi.number().required(),
    timestamp: joi.date().required(),
  }),
  handler: async function (req: Request, res: Response) {
    const vehicleId = parseInt(req.query.vehicle_id as string);
    const timestamp = new Date(req.query.timestamp as string);

    const vehicleObject = await vehicleRepo.findOneBy({ id: vehicleId });

    if (vehicleObject) {
      const vehicle = await VehicleService.getVehicleStatus(
        vehicleObject,
        timestamp
      );
      res.status(200).send({ data: vehicle });
    } else {
      res.status(404).send({ error: "Vehicle not found." });
    }
  },
};
