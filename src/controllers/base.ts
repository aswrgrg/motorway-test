import { Handler } from "express";
import joi from "joi";
export interface ControllerActionIface {
  handler: Handler;
  querySchema?: joi.ObjectSchema<any>;
  bodySchema?: joi.ObjectSchema<any>;
}
