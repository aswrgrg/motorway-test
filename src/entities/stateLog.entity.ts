import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: "stateLogs" })
export class StateLogs {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  vehicleId: number;

  @Column()
  state: string;

  @Column()
  timestamp: Date;
}
