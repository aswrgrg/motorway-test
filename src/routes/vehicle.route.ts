import { RouteIface } from "./config";
import * as VehicleController from "../controllers/vehicle.controller";

const vehicleProcessorApi: RouteIface[] = [
  {
    path: "/",
    controller: VehicleController.findStateHandler,
    method: "get",
  },
];
export default vehicleProcessorApi;
