import { ControllerActionIface } from "../controllers/base";
import vehicleProcessorApi from "./vehicle.route";
export interface RouteIface {
  path: string;
  method: "get" | "post" | "put" | "delete" | "patch";
  controller: ControllerActionIface;
}
interface RoutesConfigIface {
  [scope: string]: RouteIface[];
}

const routesConfig: RoutesConfigIface = {
  "api/vehicles": vehicleProcessorApi,
};

export default routesConfig;
