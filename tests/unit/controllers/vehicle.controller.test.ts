import { NextFunction, Response } from "express";
import { findStateHandler } from "../../../src/controllers/vehicle.controller";
import { Vehicle } from "../../../src/entities/vehicle.entity";
import VehicleService from "../../../src/services/vehicle.service";
import { vehicleRepo } from "../../../src/services/vehicle.service";

jest.mock("../../../src/services/vehicle.service");

beforeAll(async () => {
  jest.resetAllMocks();
});

afterAll(() => {
  jest.resetAllMocks();
});

describe("Vehicle Controller", () => {
  describe("GET:  CreditCards Handler", () => {
    it("should at least call VehicleService once", async () => {
      const req: any = {
        query: { vehicle_id: 123, timestamp: "2022-09-12T09:11:45.000Z" },
      };
      const res = {} as unknown as Response;
      res.send = jest.fn();
      res.status = jest.fn(() => res);
      const next = jest.fn();

      const timestamp = new Date(req.query.timestamp);
      const vehicleObject = new Vehicle();
      vehicleRepo.findOneBy = jest.fn().mockImplementationOnce(() => {
        return new Vehicle();
      });

      findStateHandler.handler(req, res as Response, next as NextFunction);

      expect(await VehicleService.getVehicleStatus).toBeCalledTimes(1);
      expect(await vehicleRepo.findOneBy).toBeCalledTimes(1);
      expect(await VehicleService.getVehicleStatus).toBeCalledWith(
        vehicleObject,
        timestamp
      );
    });

    it("should return vehicle status at given timestamp", async () => {
      const req: any = {
        query: { vehicle_id: 123, timestamp: "2022-09-12T09:11:45.000Z" },
      };
      const res = {} as unknown as Response;
      res.send = jest.fn();
      res.status = jest.fn(() => res);
      const next = jest.fn();

      const timestamp = new Date(req.query.timestamp);
      const vehicleObject = new Vehicle();
      vehicleRepo.findOneBy = jest.fn().mockImplementationOnce(() => {
        return new Vehicle();
      });

      VehicleService.getVehicleStatus = jest.fn().mockImplementation(() => [
        {
          id: 3,
          make: "VW",
          model: "GOLF",
          timestamp: "2022-09-12T10:00:00.000Z",
          state: "selling",
        },
      ]);

      const expectedReturn = [
        {
          id: 3,
          make: "VW",
          model: "GOLF",
          timestamp: "2022-09-12T10:00:00.000Z",
          state: "selling",
        },
      ];
      findStateHandler.handler(req, res as Response, next as NextFunction);

      const vehicleStatus = await VehicleService.getVehicleStatus(
        vehicleObject,
        timestamp
      );
      expect(vehicleStatus).toEqual(expectedReturn);
    });

    it("should return vehicle not found error", async () => {
      const req: any = {
        query: { vehicle_id: 123, timestamp: "2022-09-12T09:11:45.000Z" },
      };
      const res = {} as unknown as Response;
      res.send = jest.fn();
      res.status = jest.fn(() => res);
      const next = jest.fn();

      vehicleRepo.findOneBy = jest.fn().mockImplementationOnce(() => {});

      await findStateHandler.handler(
        req,
        res as Response,
        next as NextFunction
      );
      expect(res.status).toHaveBeenCalledWith(404);
    });
  });
});
