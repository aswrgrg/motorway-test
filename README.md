# MotoWay Vehicle Status Finder

## Technology Used

    $ Nodejs
    $ TypeScript
    $ Express.js
    $ PostgreSql
    $ TypeOrm

The following node version was used to build the application

    $ node --version
    v18.9.0

## Install

    $ git clone gitlab.com/aswrgrg/motorway-test
    $ cd PROJECT_TITLE
    $ npm install

## Running the project

    $ npm run start:dev

## Unit testing the project

     $ npm run test

#### Setup with docker

     $ docker compose build
     $ docker compose up

#### Endpoint example to get vehicle status

    $ http://localhost:7070/motorway/v0/api/vehicles?vehicle_id=3&timestamp=2022-09-21T09:11:45.000Z
     sample response

    {
        "data": {
            "id": 3,
            "make": "VW",
            "model": "GOLF",
            "timestamp": "2022-09-21T09:11:45.000Z",
            "state": "sold"
        }
    }
